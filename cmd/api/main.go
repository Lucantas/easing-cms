package main

import (
	"easing-cms/pkg/manager"
	"easing-cms/pkg/relation"
	"easing-cms/pkg/staticgen"
	"encoding/json"
	"log"
	"os"

	"net/http"
)

// The purpose here is to validate things created at the pkgs
// this does not replace tests, and every function should have
// it own tests
func main() {
	m := http.NewServeMux()
	m.HandleFunc("/", Index)
	m.HandleFunc("/text", TextController)
	//m.HandleFunc("/image", SaveImage)
	m.HandleFunc("/build", BuildPage)
	//m.HandleFunc("/contents", ShowContents)
	port := ":" + os.Getenv("PORT")
	http.ListenAndServe(port, m)
}

// Index is the root of this application
func Index(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello Easing Cms"))
}

func TextController(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type")
	switch r.Method {
	case "OPTIONS":
		return
	case "POST":
		SaveText(w, r)
	case "GET":
		ShowTexts(w, r)
	}
}

// SaveText receives two parameters, name and value to create
// a new Text
func SaveText(w http.ResponseWriter, r *http.Request) {
	//w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type")
	log.Println("Content received")
	var t *manager.Text
	log.Println("body", r.Body)
	if r.Body == nil {
		//w.WriteHeader(http.StatusNoContent)
		return
	}

	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		return
	}
	log.Println(t)
	t.Filter()
	relation.SaveOne(t)

	//w.WriteHeader(http.StatusUnprocessableEntity)
	//errorResponse("Invalid Content", json.NewEncoder(w))
	return
}

func ShowTexts(w http.ResponseWriter, r *http.Request) {
	relation.ShowAll(json.NewEncoder(w))
}

// SaveText receives two parameters, name and path to create
// a new Image
/* func SaveImage(w http.ResponseWriter, r *http.Request) {
	name, path := r.FormValue("name"), r.FormValue("path")
	w.Write([]byte(fmt.Sprintf("O nome da imagem é: %s e o seu valor é: %s", name, path)))

	text := db.NewImage(cms.NewImage(name, path), "")
	w.Write([]byte(fmt.Sprintf("Novo texto:%v", text)))

	text.Save()
	w.Write([]byte("Image Saved"))
} */

// BuildPage run a function in background to generate a build
// with nuxt js and send it to the aws bucket
func BuildPage(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		return
	}
	reader, _ := r.GetBody()
	log.Println(reader)
	staticgen.NewBuild()
	w.Write([]byte("build ok"))
}

// ShowContents retreives all contents in the database, images,
// texts and jobs
/* func ShowContents(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Showing all texts"))
	e := json.NewEncoder(w)
	err := e.Encode(db.GetAllTexts())
	if err != nil {
		log.Println("err")
		return
	}
	w.Write([]byte("Showing all images"))
	err = e.Encode(db.GetAllImages())
	if err != nil {
		log.Println("err")
		return
	}
} */
