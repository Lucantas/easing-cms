package staticgen

import (
	"log"
	"os"
	"os/exec"
)

// NewBuild executes a shell script to generate the build with nuxt
// and then sync the files with a bucket in aws s3
func NewBuild() Build {
	cmd := exec.Command("/bin/bash", "./assets/bash/generate_build.sh")
	output, err := cmd.CombinedOutput()
	if err != nil {
		panic(err)
	}
	log.Println(string(output))
	b := Build{
		cmd,
		output,
	}
	return b
}

// Handshake is a method implemented on variables with type Build
// it takes a string, to see if it matches with the salted secret
// returns a bool based on it
func (b Build) Handshake(s string) bool {
	// This function must do some hash, for now, it will only look
	// For a specific string stored in the environment
	if s == os.Getenv("EASING_BUILD_SECRET") {
		return true
	}
	return false
}
