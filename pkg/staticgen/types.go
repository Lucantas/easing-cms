package staticgen

import (
	"os/exec"
)

// Build is the main type of the pacakge staticgen, it holds the
// comand to execute the build generation and its output
type Build struct {
	Cmd    *exec.Cmd
	Output []byte
}
