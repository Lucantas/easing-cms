package manager

import "testing"

func TestNewText(t *testing.T) {
	// Creates a text trough NewText passing only one parameter
	// to test the first condition of the function
	text := NewText("Title")
	if text != nil {
		t.Error("Expected text to be nil because one parameter is missing")
	}
	// Creates a text trough NewText passing two parameters
	// to test the second condition
	text = NewText("Title", "<h1>Hello Go!</h1>", "Home")
	if text.Name != "Title" {
		t.Error("Expected name of the Text to be 'Title'")
	}
	if text.Value != "<h1>Hello Go!</h1>" {
		t.Error("Expected value of the Text to be '<h1>Hello Go!</h1>'")
	}

	if text.Page != "Home" {
		t.Error("Expected the page of text to be 'Home'")
	}

	text = NewText("Title", "<h1>Hello Go!</h1>", "Home", "HomeBox")

	if text.PlainValue != "Hello Go!" {
		t.Error("Expected the plain value of the text to be 'Hello Go'")
	}
	if text.Group != "HomeBox" {
		t.Error("Expected the plain value of the text to be 'HomeBox'")
	}
}
