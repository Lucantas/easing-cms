package manager

import (
	"easing-cms/pkg/relation"
)

// creating the types of the content management based
// on the db models

// Text type based on db Text model
//	type Text struct {
//		// Unique identifier, part of gorm.Model, the default primary key
//		ID uint
//		// Date of creation of the content, part of gorm.Model
//		CreatedAt time.Time
//		// Date of last update of the content, part of gorm.Model
//		UpdatedAt time.Time
//		// Name of the content
//		Name string `gorm:"size:55;not null"`
//		// Value is unique and cannot be null
//		Value string `gorm:"size:555;not null;unique"`
//		// PlainValue is the value stored after the filter function
//		PlainValue string `gorm:"size:555"`
//		// Group is the column group_name it not specified, it assumes
//		// the value as unset
//		Group string `gorm:"default:'unset';unique;not null;column:group_name"`
//	}
type Text relation.Text

// Post is a most complex content, it's used to create blog posts,
// articles, and also can be used to create a Jobs page, since a Job
// Is basically a post built with the job information
//	type Post struct {
//		// Holds every property of a gorm.Model
//		gorm.Model
//		// tags is a slice of Tag, it is used to filter the post
//		// and make it easier to find
//		Tags     []Tag `gorm:"many2many:content_tags;"`
//		Name     string         `gorm:"size:55;not null"`
//		Title    string         `gorm:"not null"`
//		Subtitle string
//		Category string `gorm:"not null"`
// // 	Lead     string `gorm:"size:355"`
// // 	Text     string `gorm:"size:55;not null"`
// // 	Images   []Image
// // }
type Post relation.Post

// Image is a small piece of content, it is used in two cases
// As a single element in the page, along with some other images
// and text contents, or it can be used as a part of a Post
// type Image struct {
// 	ID        uint
// 	CreatedAt time.Time
// 	UpdatedAt time.Time
// 	Name      string `gorm:"size:55;not null"`
// 	PathName  string `gorm:"not null"`
// 	Caption   string
// 	Alt       string `gorm:"not null"`
// 	Tags      []Tag  `gorm:"many2many:content_tags;"`
// 	Group     string `gorm:"default:'null'"`
// }
type Image relation.Image

// Tag is used to make it easy to find images and posts
// type Tag struct {
// 	ID        uint
// 	CreatedAt time.Time
// 	UpdatedAt time.Time
// 	Name      string `gorm:"size:75;not null"`
// 	Posts     []Post `gorm:"many2many:content_tags;"`
// }
type Tag relation.Tag
