package manager

import (
	"github.com/microcosm-cc/bluemonday"
)

// Initialization

// NewText needs to receive at least 2 strings, and it might
// receive up to 4
// The strings that must be passed to this functions are:
// Name, Value and Page
// The fourth string parameter is used as group name
func NewText(props ...string) *Text {
	if len(props) < 3 {
		return nil
	}
	t := newText(props[0], props[1], props[2])
	if len(props) > 3 {
		t.EditGroup(props[3])
	}
	t.Filter()
	return t
}

func newText(name string, value string, page string) *Text {
	t := Text{
		Name:  name,
		Value: value,
		Page:  page,
	}
	return &t
}

// EditGroup takes a string to change the value of a pointer to Text
func (t *Text) EditGroup(group string) {
	t.Group = group
}

// Methods
// Filter
func (t *Text) Filter() {
	t.PlainValue = trimHTML(t.Value)
}

func trimHTML(md string) string {
	// this function clear the markdown from the content
	return bluemonday.StrictPolicy().Sanitize(md)
}
