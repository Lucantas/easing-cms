package relation

import (
	"time"

	"github.com/jinzhu/gorm"
	// importing postgres dialect for the models
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type conn struct {
	Host, Port, User, DBName, Password string
	SSL                                bool
}

// Text is a small piece of content, most of the cms plan is to
// cover all the text contents in a website, most likely a landing page
// or so
type Text struct {
	// Unique identifier, part of gorm.Model, the default primary key
	ID uint
	// Date of creation of the content, part of gorm.Model
	CreatedAt time.Time
	// Date of last update of the content, part of gorm.Model
	UpdatedAt time.Time
	// Name of the content
	Name string `gorm:"size:35;not null"`
	// Value is unique and cannot be null
	Value string `gorm:"size:555;not null;unique"`
	// PlainValue is the value stored after the filter function
	PlainValue string `gorm:"size:555"`
	// Page is a string containing the name of the page
	Page string `gorm:"size:35;not null"`
	// Group is the column group_name it not specified, it assumes
	// the value as unset
	Group string `gorm:"default:'unset';not null;column:group_name"`
}

// Post is a most complex content, it's used to create blog posts,
// articles, and also can be used to create a Jobs page, since a Job
// Is basically a post built with the job information
type Post struct {
	// Holds every property of a gorm.Model
	gorm.Model
	// tags is a slice of Tag, it is used to filter the post
	// and make it easier to find
	Tags     []Tag  `gorm:"many2many:content_tags;"`
	Name     string `gorm:"size:55;not null"`
	Title    string `gorm:"not null"`
	Subtitle string
	Category string `gorm:"not null"`
	Lead     string `gorm:"size:355"`
	Text     string `gorm:"size:55;not null"`
	Images   []Image
}

// Image is a small piece of content, it is used in two cases
// As a single element in the page, along with some other images
// and text contents, or it can be used as a part of a Post
type Image struct {
	ID        uint
	CreatedAt time.Time
	UpdatedAt time.Time
	Name      string `gorm:"size:55;not null"`
	PathName  string `gorm:"not null"`
	Caption   string
	Alt       string `gorm:"not null"`
	Tags      []Tag  `gorm:"many2many:content_tags;"`
	Group     string `gorm:"default:'null'"`
}

// Tag is used to make it easy to find images and posts
type Tag struct {
	ID        uint
	CreatedAt time.Time
	UpdatedAt time.Time
	Name      string `gorm:"size:75;not null"`
	Posts     []Post `gorm:"many2many:content_tags;"`
}
