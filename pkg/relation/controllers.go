package relation

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/jinzhu/gorm"
)

func Migrate(db *gorm.DB) {
	db.AutoMigrate(&Text{}, &Post{}, &Image{}, &Tag{})
}

func SaveOne(content interface{}) {
	db := dbConn()
	defer db.Close()
	Migrate(db)
	switch v := content.(type) {
	default:
		db.NewRecord(&v)
		db.Create(v)
	}
}

func ShowAll(e *json.Encoder) {
	db := dbConn()
	defer db.Close()
	t := []Text{}
	db.Find(&t)
	//subs, err := json.Marshal(s)
	err := e.Encode(t)
	if err != nil {
		log.Println(err)
	}
	return
}

func dbConn() *gorm.DB {
	c := conn{
		Host:     os.Getenv("db_host"),
		Port:     os.Getenv("db_port"),
		User:     os.Getenv("db_user"),
		DBName:   os.Getenv("db_name"),
		Password: os.Getenv("db_pass"),
		SSL:      false,
	}
	db, err := gorm.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s", c.Host, c.Port, c.User, c.DBName, c.Password))
	if err != nil {
		panic(err)
	}
	return db
}
