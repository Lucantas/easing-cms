package mux

import (
	"net/http"

	"github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
)

type query struct{}

func (_ *query) Hello() string { return "Hello, world!" }

// POST is the main Handler of the api
type POST struct {
	Handle func(w http.ResponseWriter, r *http.Request)
}

type r relay.Handler

type graph graphql.Schema
